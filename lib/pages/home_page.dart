import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meditation_app/pages/choose_topic_page.dart';
import 'package:meditation_app/ultis/theme.dart';
import 'package:meditation_app/widgets.dart/responsive_builder.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: SafeArea(
        child: ResponsiveBuilder(
          portrait: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: FractionallySizedBox(
                  heightFactor: 0.6,
                  child: _backgroundWidget(),
                ),
              ),
              FractionallySizedBox(
                heightFactor: 0.4,
                child: _bodyWidget(),
              ),
              Align(
                alignment: const Alignment(0, 0.95),
                child: buttonGet(context),
              )
            ],
          ),
          landscape: Row(
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.topCenter,
                  child: FractionallySizedBox(
                    heightFactor: 0.7,
                    child: _bodyWidget(),
                  ),
                ),
              ),
              Expanded(
                  child: Stack(
                children: [
                  _backgroundWidget(),
                  Align(
                      alignment: Alignment(0, 0.8), child: buttonGet(context)),
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }

  Widget _backgroundWidget() {
    return SvgPicture.asset('assets/images/start.svg');
  }

  Widget _bodyWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Expanded(
          flex: 3,
          child: Container(
            alignment: Alignment(0, -0.5),
            child: Image.asset(
              'assets/images/logo.png',
              fit: BoxFit.cover,
              height: 40,
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Hi Afsar, Welcome',
                style: PrimaryFont.medium(30),
              ),
              Text(
                'to Silent Moon',
                style: PrimaryFont.thin(30),
              )
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.only(left: 35, right: 35),
            child: Text(
              "Explore the app, Find some peace of mind to prepare for meditation.",
              style: PrimaryFont.light(16)
                  .copyWith(color: Colors.white.withOpacity(0.7)),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
  }

  Widget buttonGet(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 40, right: 40),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ChooseTopicPage(),
          ));
        },
        child: Container(
          width: double.infinity,
          height: 55,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color: Colors.white.withOpacity(0.8),
          ),
          child: Center(child: Text('GET STARTED')),
        ),
      ),
    );
  }
}
