import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meditation_app/data/topic_storage.dart';
import 'package:meditation_app/pages/reminder_page.dart';
import 'package:meditation_app/ultis/theme.dart';
import 'package:meditation_app/widgets.dart/responsive_builder.dart';

import '../data/models/topic_model.dart';

final topicStorage = RemoteTopicStorage();

class ChooseTopicPage extends StatelessWidget {
  const ChooseTopicPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ResponsiveBuilder(
          portrait: Column(
            children: [
              const Expanded(
                flex: 1,
                child: _header(),
              ),
              Expanded(
                flex: 3,
                child: _listTopics(),
              ),
            ],
          ),
          landscape: Row(
            children: [
              Expanded(
                flex: 1,
                child: Column(
                  children: const [
                    Expanded(child: _header()),
                    Spacer(),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: _listTopics(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _listTopics() {
    return FutureBuilder<List<Topic>>(
      future: topicStorage.load(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        }
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        }
        final topics = snapshot.data!;
        return MasonryGridView.count(
            itemCount: topics.length,
            crossAxisCount: 2,
            crossAxisSpacing: 16,
            padding: EdgeInsets.fromLTRB(16, 0, 16, 16),
            mainAxisSpacing: 16,
            itemBuilder: (condex, index) {
              final topic = topics[index];
              return InkWell(
                onTap: (() {
                  Navigator.of(context).pushNamed('$ReminderPage');
                }),
                child: DecoratedBox(
                  decoration: BoxDecoration(
                      color: Color(int.parse(topic.color)),
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    children: [
                      SvgPicture.asset(topic.thumbnail),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          topic.title,
                          style: PrimaryFont.bold(18).copyWith(
                              color: Color(int.parse(topic.titleColor))),
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      )
                    ],
                  ),
                ),
              );
            });
      },
    );
  }
}

class _header extends StatelessWidget {
  const _header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Spacer(),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: FittedBox(
                    child: Text(
                      'What Brings you',
                      style: PrimaryFont.bold(28),
                    ),
                  ),
                ),
                Expanded(
                  child: FittedBox(
                    child: Text(
                      'to Silent Moon?',
                      style: PrimaryFont.light(28),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 1,
                  child: FittedBox(
                    child: Text(
                      'choose a topic to focus on',
                      style: PrimaryFont.light(28)
                          .copyWith(color: const Color(0xFFA1A482)),
                    ),
                  ),
                ),
                const Spacer(
                  flex: 2,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
