import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:meditation_app/ultis/theme.dart';

const dayOfWeeks = ["SU", "M", "T", "W", "TH", "F", "S"];

class ReminderPage extends StatelessWidget {
  const ReminderPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: const [
            Expanded(
              flex: 2,
              child: _TimerHeader(
                title: 'What time would you\nlike to mediatate?',
                subTitle:
                    'Any time you can choose but We recommend\nfirst thing in th morning.',
              ),
            ),
            Expanded(
              flex: 3,
              child: _TimerSelect(),
            ),
            Expanded(
              flex: 2,
              child: _TimerHeader(
                title: 'Which day would you\nlike to meditate?',
                subTitle:
                    'Everyday is best, but we recommend \npicking at lease five.',
              ),
            ),
            Expanded(
              child: _DaySelect(),
            ),
            Expanded(
              flex: 2,
              child: _Buttons(),
            ),
          ],
        ),
      ),
    );
  }
}

class _TimerSelect extends StatelessWidget {
  const _TimerSelect({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 8, 8, 24),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: const Color(0xFFF5F5F9),
          borderRadius: BorderRadius.circular(20),
        ),
      ),
    );
  }
}

class _DaySelect extends StatelessWidget {
  const _DaySelect({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: List.generate(7, (index) {
          return Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: DecoratedBox(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.transparent,
                  border: Border.all(color: const Color(0xFFA1A4B2)),
                ),
                child: Center(child: Text(dayOfWeeks[index])),
              ),
            ),
          );
        }),
      ),
    );
  }
}

class _Buttons extends StatelessWidget {
  const _Buttons({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: MaterialButton(
              onPressed: () {},
              child: Text(
                'SAVE',
                style: PrimaryFont.medium(14),
              ),
              color: kPrimaryColor,
              textColor: Colors.white,
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(38)),
            ),
          ),
          Expanded(
            child: TextButton(
              onPressed: () {},
              child: Text(
                'NO THANKS',
                style: PrimaryFont.medium(14)
                    .copyWith(color: Colors.black.withOpacity(0.7)),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class _TimerHeader extends StatelessWidget {
  const _TimerHeader({
    Key? key,
    required this.title,
    required this.subTitle,
  }) : super(key: key);

  final String title;
  final String subTitle;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: FractionallySizedBox(
              alignment: Alignment.topLeft,
              heightFactor: 0.75,
              child: FittedBox(
                child: Text(
                  title,
                  style: PrimaryFont.bold(24).copyWith(height: 1.35),
                ),
              ),
            ),
          ),
          Expanded(
            child: FractionallySizedBox(
              heightFactor: 0.6,
              alignment: Alignment.topLeft,
              child: FittedBox(
                alignment: Alignment.topLeft,
                child: Text(
                  subTitle,
                  style: PrimaryFont.light(16)
                      .copyWith(color: const Color(0xFFA1A4B2), height: 1.65),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
