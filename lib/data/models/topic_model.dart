// class Topic {
//   final String title;
//   final String thumbnail;
//   final int color;
//   final int titleColor;

//   Topic(
//       {required this.title,
//       required this.thumbnail,
//       required this.color,
//       required this.titleColor});
// }

// To parse this JSON data, do
//
//     final topic = topicFromJson(jsonString);

// To parse this JSON data, do
//
//     final topic = topicFromJson(jsonString);

import 'dart:convert';

List<Topic> topicFromJson(String str) =>
    List<Topic>.from(json.decode(str).map((x) => Topic.fromJson(x)));

String topicToJson(List<Topic> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Topic {
  Topic({
    required this.title,
    required this.thumbnail,
    required this.color,
    required this.titleColor,
  });

  String title;
  String thumbnail;
  String color;
  String titleColor;

  factory Topic.fromJson(Map<String, dynamic> json) => Topic(
        title: json["title"],
        thumbnail: json["thumbnail"],
        color: json["color"],
        titleColor: json["titleColor"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "thumbnail": thumbnail,
        "color": color,
        "titleColor": titleColor,
      };
}
