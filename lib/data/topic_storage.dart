import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'models/topic_model.dart';
import 'package:http/http.dart' as http;

abstract class TopicStorage {
  Future<List<Topic>> load();
}

class AssetTopicStorage extends TopicStorage {
  @override
  Future<List<Topic>> load() async {
    if (kDebugMode) {
      await Future.delayed(Duration(seconds: 2));
    }
    final jsonContent = await rootBundle.loadString("mock/topics/topics.json");
    return topicFromJson(jsonContent);
  }
}

class RemoteTopicStorage extends TopicStorage {
  var dio = Dio();
  List<Topic> listTopics = [];
  @override
  Future<List<Topic>> load() async {
    if (kDebugMode) {
      await Future.delayed(Duration(seconds: 2));
    }
    //using dio
    // final response =
    //     await Dio().get('https://nodejs-simpleserver.herokuapp.com/');

    // final List<dynamic> responseBody = response.data;
    // return responseBody.map((e) => Topic.fromJson(e)).toList();

    //handle error
    try {
      //404
      final response =
          await dio.get('https://nodejs-simpleserver.herokuapp.com/');
      final List<dynamic> responseBody = response.data;
      listTopics = responseBody.map((e) => Topic.fromJson(e)).toList();
    } catch (e) {
      if (e is DioError) {
        throw e.error.toString();
      } else {
        throw e.toString();
      }
    }
    return listTopics;

    //
    //using http
    // final response =
    //     await http.get(Uri.parse('https://nodejs-simpleserver.herokuapp.com/'));
    // if (response.statusCode == 200)
    //   return topicFromJson(response.body);
    // else
    //   throw Exception('Failed to load post');
    //
  }
}
