import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meditation_app/pages/choose_topic_page.dart';
import 'package:meditation_app/ultis/theme.dart';

import 'pages/home_page.dart';
import 'pages/reminder_page.dart';

void main() => runApp(
      DevicePreview(
        enabled: !kReleaseMode,
        builder: (context) => MyApp(), // Wrap your app
      ),
    );

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: kPrimaryColor,
      ),
      useInheritedMediaQuery: true,
      locale: DevicePreview.locale(context),
      builder: DevicePreview.appBuilder,
      darkTheme: ThemeData.dark(),
      initialRoute: '$HomePage',
      routes: {
        '$HomePage': (context) => HomePage(),
        '$ChooseTopicPage': (context) => ChooseTopicPage(),
        '$ReminderPage': (context) => ReminderPage(),
      },
    );
  }
}
