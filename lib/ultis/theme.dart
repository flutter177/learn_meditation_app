import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF8E97fD);

class PrimaryFont {
  static TextStyle thin(double size) {
    return TextStyle(
      fontSize: size,
      fontWeight: FontWeight.w100,
      color: Colors.white.withOpacity(0.7),
    );
  }

  static TextStyle medium(double size) {
    return TextStyle(
      fontSize: size,
      fontWeight: FontWeight.w500,
      color: Colors.white,
    );
  }

  static TextStyle bold(double size) {
    return TextStyle(
      fontSize: size,
      fontWeight: FontWeight.w700,
    );
  }

  static TextStyle light(double size) {
    return TextStyle(
      fontSize: size,
      fontWeight: FontWeight.w300,
    );
  }
}

extension GetOrientation on BuildContext {
  Orientation get orientation => MediaQuery.of(this).orientation;
}
